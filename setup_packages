#!/bin/bash

# Install packages for a development environment.
# This list was made on Ubuntu Bionic

# terminal workstation
apt-get install -y \
    ncurses-term tmux gpm fbterm fbset \
    bash zsh zsh-syntax-highlighting \
    findutils parallel \
    vim \
    git tig \
    exuberant-ctags cscope global \
    wdiff diffstat \
    sloccount shellcheck \
    grep silversearcher-ag ripgrep \
    lsof jnettop telnet traceroute whois ngrep tcpdump nmap \
    gnupg gpg-agent ssh-askpass \
    openssh-client unison \
    nfs-common sshfs \
    curl wget \
    htop iotop \
    ranger less tree trash-cli \
    tar gzip zip unzip bzip2 unrar p7zip fuseiso

# language runtimes and checkers
apt-get install -y \
    make gcc build-essential cppcheck uncrustify \
    python3.7 python3-distutils \
    sqlite3 \
    shellcheck \
    tidy \
    libxml2-utils

# X workstation
apt-get install -y \
    qtile xterm yeahconsole \
    gdebi baobab file-roller redshift gnome-tweak-tool \
    xclip xsel \
    vim-gtk3 meld \
    git-gui \
    gdb winpdb \
    seahorse keepassx \
    thunderbird \
    firefox chromium-browser

# desktop
apt-get install -y gimp mypaint inkscape dia grabc
apt-get install -y gnumeric abiword
apt-get install -y gmpc ncmpcpp eog vlc totem
apt-get install -y fontforge gramps

# server
apt-get install -y openssh-server molly-guard
apt-get install -y mpd beets

# apt-get install -y kubectx
# Not handled:
# - kubectl
# - minikube

# Thinkpad packages
vendor=$(cat /sys/class/dmi/id/sys_vendor)
if [ "$vendor" = "LENOVO" ]; then 
	apt-get install tpb acpi-support
	apt-get install powertop
fi
unset vendor

# load settings for meld, which were dumped with dconf dump /org/gnome/meld/
[ -e meld-conf.ini ] && dconf load /org/gnome/meld/ < meld-conf.ini

# use xterm as default terminal, x-terminal-emulator
update-alternatives --set x-terminal-emulator /usr/bin/xterm
