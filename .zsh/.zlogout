# $ZDOTDIR/.zlogout
# Sourced when login shells exit (e.g. tty, ssh)

# Clear screen when leaving console for privacy
if (( $SHLVL == 1 )); then
    clear
fi
