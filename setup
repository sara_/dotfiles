#!/bin/bash

# Shell function for internal error handling
die () {
    echo "$1"
    exit 1
}

# We'll need to change system packages, so check if we have that access.
# If we don't have system packages then we might not be able to do the rest.
# (for example, can't set up vim or zsh if we don't have them.)
# So just bail if we can't get the access to do that.
if [ "$(id -u)" -ne 0 ]; then
    die "Run this script with sudo. It needs to manage system packages."
fi

# Figure out what user we're setting up for.
# Commands will be run as a certain user to ensure they have the right
# permissions, e.g. that directories created belong to the user, not to root.
user=""

# We expect a username to be given, and if so, we will use that.
if [ ! -z "$1" ]; then
    user="$1"
# If no username is given, we can try to guess from who's running sudo.
else
    user="$SUDO_USER"
fi
# Otherwise, complain. Resist any urge to use results of whoami.
# if this is being run as root and they want a root setup, have to specify it.
if [ -z "$user" ]; then
    die "If not running with sudo, specify the user, like $0 sammy"
fi
# Make sure the user isn't something nonsensical
if ! id -u "$user" >/dev/null  2>&1 ; then
    die "user appears not to exist: $user"
fi

# We likely need certain directories for the rest to work.
# Just make all of those up front.
sudo -u "${user}" bash ./setup_directories || die "failed in setup_directories"

# Add symlinks to configs in this repo.
# Do this before running any package stuff, so the symlinks already exist in
# case something else is run which adds dotfiles.
sudo -u "${user}" bash ./setup_symlinks || die "failed in setup_symlinks"

# Add symlinks to executables from this repo.
# Do early in case we use any scripts locally included from this repo
sudo -u "${user}" bash ./setup_bin || die "failed in setup_bin"

# Remove unwanted packages automatically supplied by the distro
# Do this first, in case some might be deps of ones we ask for later;
# at least nothing requested will break then
bash ./remove_unwanted_packages || die "failed in remove_unwanted_packages"

# Install distro packages used for the development environment
bash ./setup_packages || die "failed in setup_packages"

# TEMPORARY!
apt-get install -y python3-pip

sudo -u "${user}" bash ./setup_python_packages
sudo -u "${user}" bash ./setup_npm_packages
sudo -u "${user}" bash ./setup_nonpackages

# Configure especially interesting programs
sudo -u "${user}" bash ./setup_vim
sudo -u "${user}" bash ./setup_zsh
