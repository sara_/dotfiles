# $ZDOTDIR/.zprofile
# Sourced before .zshrc for interactive login shells (tty, ssh - not xterms)

# Load .profile in compatibility mode
emulate sh
. ~/.profile
emulate zsh
