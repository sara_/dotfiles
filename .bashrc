# .bashrc is for non-login shells.
# People often source it from .bash_profile
# Ubuntu says: see /usr/share/doc/bash/examples/startup-files for examples

# TODO: does this need to source ~/.profile?

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in system files under /etc)
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# Keep $LINES and $COLUMNS updated with window size after each command.
shopt -s checkwinsize
# & suppresses duplicates, space in front of a command does it quietly
export HISTIGNORE="&:ls:bg:fg:exit:clear:history *:[ \t]*"
# Giant history
export HISTSIZE=1000
# Append to history file instead of overwriting, for concurrent sessions
shopt -s histappend
# Make sure history isn't mashed up with other shells using other formats
export HISTFILE=~/.bash_history
# enable storing of multi-line commands as a single command in history
shopt -s cmdhist
# set default prompt reflecting username, host, path and whether I am root
export PS1="[$ \u @ \H \w] "

# case-insensitive tab completion
set completion-ignore-case
# show all ambiguous matches
set show-all-if-ambiguous
# show symlinked dirs
set show-all-symlinked-directories
# sudo autocompletion
complete -cf sudo
# tab-completion of hostnames after @   http://dotfiles.org/~samba/.bashrc
shopt -s hostcomplete
# SSH Auto Completion of Remote Hosts, http://dotfiles.org/~jnunemaker/.bash_profile
if [ -f ~/.ssh/known_hosts ]; then
  SSH_COMPLETE=( $(cat ~/.ssh/known_hosts | cut -f 1 -d ' ' | sed -e s/,.*//g | uniq | egrep -v [0123456789]) )
  complete -o default -W "${SSH_COMPLETE[*]}" ssh
fi

# Here I source aliases from a separate file.
if [ -e $HOME/.alias ]; then
    [ -n "$PS1" ] && . $HOME/.alias
fi

[ -e ~/.fix_terminal ] && source ~/.fix_terminal

if command -v vex > /dev/null ; then
    eval "$(vex --shell-config bash)"
fi
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
