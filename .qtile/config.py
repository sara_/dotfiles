import logging
import subprocess

from libqtile.config import Key, Screen, Group, Match, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook

mod = "mod4"
terminal = "uxterm"
browser = "firefox"
music_player = "gmpc"
volume = "pavucontrol"
printer = "system-config-printer"


keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),

    # (Same, but more 'normal' keys - I don't use alt-tab for other things)
    Key(["mod1"], "Tab", lazy.layout.down()),
    Key(["mod1", "shift"], "Tab", lazy.layout.up()),

    # Reorder windows up or down within current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down()),
    Key([mod, "control"], "j", lazy.layout.shuffle_up()),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    # Split or unsplit one side of the stack.
    #   split: all windows in stack shown at once.
    #   unsplit: one window from stack shown at once.
    # n.b.: this is really very useful
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),

    Key([mod], "Return", lazy.spawn(terminal)),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),

    Key([mod], "w", lazy.window.kill()),

    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),
    Key([mod], "semicolon", lazy.spawncmd()),

    # Cycle through groups with Ctrl-Alt-Left/Right
    Key(["mod1", "control"], "Left", lazy.screen.prevgroup(skip_managed=True)),
    Key(["mod1", "control"], "Right", lazy.screen.nextgroup(skip_managed=True)),

    Key([mod], "slash", lazy.switchgroup()),

    # for tree layout
    Key([mod], "h", lazy.layout.collapse_branch()),
    Key([mod], "l", lazy.layout.expand_branch()),

    Key([], "XF86MonBrightnessUp", lazy.spawn('xbacklight -inc 10')),
    Key([], "XF86MonBrightnessDown", lazy.spawn('xbacklight -dec 10')),
]

# Groups are workspaces. Each window belongs to one Group.
# This name is used directly by qtile.
# (the defaults like asdf are hard for me to chord with super. Maybe my hands
# are too big.)
groups = [
    Group(i) for i in "1234567890"
]
groups.extend([
    Group("m", matches=[
        Match(wm_class=["gmpc", "Gmpc"])
    ]),
    Group("g", matches=[
        Match(wm_class=["Steam"])
    ]),
])

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        # mod1 + shift + letter of group =
        # switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
    ])

text_options = {
    'font': 'Liberation Sans',
    'foreground': 'AAAAAA',
    'background': '1D1D1D',
    'fontsize': 16,
    'padding': 3,
}

layout_options = {
    'border_normal': '#444444',
    'border_focus': '#888888',
    'border_width': 1,
}

# Layouts are algorithms for arranging windows on a Screen.
# Here's where we configure layouts to cycle through;
# the key for cycling through them is separate
layouts = [
    # One window at a time, 'maximized'.
    layout.Max(),

    # like Max with a sort of task bar on one side.
    # layout.TreeTab(panel_width=100, fontsize=10),

    # Stack is the Qtile default layout:
    # Vertical columns, each can either show one window
    # or tile all the windows in that stack (split?)
    # according to keybinds.
    # However, it doesn't seem to have ratio resizing?
    layout.Stack(num_stacks=2),

    # Main pane on one side, secondary panes on other,
    # but no split/unsplit so why?
    # layout.MonadTall(**layout_options),

    # Try to set all windows to tiles with same w/h ratio,
    # like the Brady Bunch.
    # or a poor man's Expose
    layout.RatioTile(**layout_options),

    # layout.Tile(ratio=0.25, **layout_options),

    # One active Window, sorta thumbnails at right.
    # layout.Zoomy(**layout_options),

    # Tile - no docstring.
    # layout.Slice()  # fails?
]

sep_color = "444444"
widget_defaults = dict(
    font='sans',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

# Screen objects represent physical screens,
# this is where widgets are configured
screens = [
    Screen(
        bottom=bar.Bar([
                # Room for system/notification icons
                widget.Systray(),

                # Tells the name of what has focus
                # but not compatible with TaskList
                # widget.WindowName(**text_options),

                # Can't have a WindowName also because that's a STRETCH
                # widget and you can only have one of those.
                widget.TaskList(**text_options),

                # Indicate what's on clipboard
                # widget.Clipboard(max_width=5, timeout=2, **text_options),

                # Prompt for things like spawning processes or switching
                # groups, usually invisible
                widget.Prompt(**text_options),

                # Name the currently applied layout - can click to switch
                widget.CurrentLayout(**text_options),

                # List of workspaces
                widget.GroupBox(
                    urgent_alert_method='text',
                    this_current_screen_border='444444',
                    **text_options
                ),

                widget.Volume(),

                widget.BatteryIcon(),

                widget.Sep(foreground=sep_color),

                widget.Clock(
                    format='%Y-%m-%d %a %I:%M %p',
                    **text_options
                ),

                # widget.Sep(foreground=sep_color),
                # widget.TextBox("default config", name="default"),

                # # Does not seem to work
                # widget.YahooWeather(
                #     woeid="12590014",
                #     metric=False,
                #     format="{condition_text}, {condition_temp}°", **text_options
                # ),
            ],
            # 30px high
            30,
        ),
    ),
    Screen(
        bottom=bar.Bar([
            widget.GroupBox(**text_options)
        ], 30)
    )
]

# Define mouse actions
mouse = [
    # Drag floating layouts.
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

# Let some things just use the floating layout
@hook.subscribe.client_new
def floating_dialogs(window):
    dialog = window.window.get_wm_type() == 'dialog'
    transient = window.window.get_wm_transient_for()
    # if "zim" in window.window.get_wm_class():
    #     window.floating = False
    if dialog or transient:
        window.floating = True


@hook.subscribe.startup
def on_startup():
    pass


def main(qtile):
    # set logging level
    qtile.cmd_info()

dgroups_key_binder = None
dgroups_app_rules = []
# I commented this out before? to define main I think
# main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    #{'wmiconname': 'Warning'},
    {'wmclass': 'toolbar'},
    # inkscape
    {'wmclass': 'Inkscape'},
    # gitk
    {'wmclass': 'confirmreset'},
    {'wmclass': 'makebranch'},
    {'wmclass': 'maketag'},
    {'wname': 'branchdialog'},
    # gpg key password entry
    {'wname': 'pinentry'},
    # ssh gui
    {'wmclass': 'ssh-askpass'},
])

auto_fullscreen = True
focus_on_window_activation = "smart"

# wmname = "qtile"
# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, github issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

# For running inside Gnome
import subprocess
import os
from libqtile import hook

# Try to make qtile cooperate with gnome
@hook.subscribe.startup
def dbus_register():
    id = os.environ.get("DESKTOP_AUTOSTART_ID")
    if not id:
        return
    subprocess.Popen([
        "dbus-send",
        "--session",
        "--print-reply",
        "--dest=org.gnome.SessionManager",
        "/org/gnome/SessionManager",
        "org.gnome.SessionManager.RegisterClient",
        "string:qtile",
        "string:" + id
    ])
