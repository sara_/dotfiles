from ptpython.layout import CompletionVisualisation


def configure(repl):
    repl.enable_mouse_support = True
    repl.vi_mode = True
    repl.confirm_exit = False
    repl.use_code_colorscheme("native")
    repl.color_depth = 'DEPTH_24_BIT'
    repl.insert_blank_line_after_output = False
    repl.show_meta_enter_message = False
    repl.show_docstring = True
    repl.highlight_matching_parenthesis = True
    repl.completion_visualization = CompletionVisualisation.POP_UP

    # press up to filter history on what's entered so far
    repl.enable_history_search = True

    # apparently required for history search
    repl.complete_while_typing = False
