# Only wait 10ms for multi-character key sequences
KEYTIMEOUT=1

# Force vi-style key bindings regardless of the value of $EDITOR.
bindkey -v

# Load keys from terminfo
typeset -g -A key

load_terminfo_keys() {
    key[Home]="$terminfo[khome]"
    key[End]="$terminfo[kend]"
    key[Insert]="$terminfo[kich1]"
    key[Backspace]="$terminfo[kbs]"
    key[Delete]="$terminfo[kdch1]"
    key[Up]="$terminfo[kcuu1]"
    key[Down]="$terminfo[kcud1]"
    key[Left]="$terminfo[kcub1]"
    key[Right]="$terminfo[kcuf1]"
    key[PageUp]="$terminfo[kpp]"
    key[PageDown]="$terminfo[knp]"
}
load_terminfo_keys

# "make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid"
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
    function zle-line-init () {
        # set cursor keys to 'application' mode
        echoti smkx
    }
    function zle-line-finish () {
        # reset cursor keys to normal or 'cursor' mode
        echoti rmkx
    }
    zle -N zle-line-init
    zle -N zle-line-finish
fi

# Type some text and hit up to find past commands starting with that text.
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "$key[Up]" up-line-or-beginning-search
bindkey "$key[Down]" down-line-or-beginning-search

# In the completion menu, hitting enter once runs the command,
# don't have to hit it twice. The option is already selected.
# If you are making a list, hit space instead of enter between items.
bindkey -M menuselect '^M' .accept-line
# moving through whole pages in menu selection
bindkey -M menuselect "$key[PageUp]" backward-word
bindkey -M menuselect "$key[PageDown]" forward-word
# vi-style key for same
bindkey -M menuselect '^F' forward-word
bindkey -M menuselect '^B' backward-word
# bail from menu selection with escape key    
bindkey -M menuselect '\e' send-break

# TODO: arrow keys shouldn't cancel interactive, but navigate around.
# bindkey -M menuselect "$key[Up]" up-line-or-history
# bindkey -M menuselect "$key[Down]" down-line-or-history

# Used for working on completion scripts - give current context, etc.
bindkey ^K _complete_help

