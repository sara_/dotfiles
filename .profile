# ~/.profile: executed by the command interpreter for login shells: tty and ssh
# logins. Unlike .*shrc, it may also be executed by the user during X login,
# and should only be executed once for each session, which might contain any
# number of interactive shell invocations (different windows, nested
# invocations, etc.)
# This file is not read by bash, if ~/.bash_profile or ~/.bash_login exists.
# zsh only reads ~/.zprofile (but mine sources ~/.profile in sh mode).
# If you need x-specific stuff done, it could be done in ~/.xprofile.

# define a helper for changing ~/foo to $HOME/foo
expand_tilde() {
    case "$1" in
        "~/"*)
            stripped="${1#\~/}"
            [ "$1" != "$stripped" ] && stripped="$HOME/$stripped" ;;
        "~"*)
            (>&2 echo "error: ~username syntax not supported")
            exit 1 ;;
        *) stripped="$1" ;;
    esac
    printf '%s' "$stripped"
}

# define a helper for reading $PATH type variables from text files.
pathfile () {
    if [ -z "$1" ] ; then
        (>&2 echo "error: provide a path to read from")
        exit 1
    fi
    if [ ! -f "$1" ]; then
        (>&2 echo "error: could not read from specified path file: $1")
        exit 1
    fi
    result=""
    # we intentionally do this in a way that strips whitespace
    # 1. paths can't have spaces at beginning, so be forgiving
    # 2. spaces at end will be overlooked and cause mysteries
    while read -r line; do
        case $line in
            # reject comments
            "#"*) ;;
            # accept anything that isn't just spaces
            *[![:space:]]*)
                # change ~ to $HOME
                # so the input can be a plain text file and we don't use eval
                expanded=$(expand_tilde "$line")
                result="${result:+${result}:}${expanded}" ;;
            # reject everything else
            *) ;;
        esac
    done < "$1"
    if [ ! -z "$result" ]; then
        if [ ! -z "$2" ]; then
            echo "$2:$result"
        else
            echo "$result"
        fi
    else
        if [ ! -z "$2" ]; then
            echo "$2"
        fi
    fi
}

export EDITOR="vim"
export VISUAL="$EDITOR"
export GIT_EDITOR="$EDITOR"
export SVN_EDITOR="$EDITOR"
# use --no-init so when you exit, the text remains on screen
export PAGER='less --no-init'
export MANPAGER="$PAGER"

# Read pathfiles - plain text files that are easy to edit
path_path="$HOME/dotfiles"
PATH="$(pathfile "$path_path/path" "$PATH")"
export PATH
# the leading colon is to avoid losing the system man pages
MANPATH=":$(pathfile "$path_path/manpath" "$MANPATH")"
export MANPATH
NODE_PATH="$(pathfile "$path_path/node_path" "$NODE_PATH")"
export NODE_PATH

# virtualenvwrapper: set where virtualenv directories are put
export WORKON_HOME="$HOME/.virtualenvs"
# virtualenvwrapper: location of development project directories
export PROJECT_HOME="$HOME/projects"
# enable tab completion in ordinary Python interpreter, etc.
export PYTHONSTARTUP="$HOME/.pystartup"
# grep matches in bright yellow
export GREP_COLOR='1;33'

# set the LS_COLORS using the more friendly ~/.dircolors config file
if command -v dircolors > /dev/null ; then
    if [ -f ~/.dircolors ]; then
        eval "$(dircolors --sh ~/.dircolors)"
    else
        eval "$(dircolors --sh)"
    fi
fi

# Set up options for less.
# - preserve ANSI colors (as output by pygmentize, git diff, etc.)
# - default to case insensitive search unless uppercase in pattern
# - get killed by Ctrl-C like most other programs
# - use a left column to indicate which parts matched the search
# - use prompt which tells position in file
# - don't wrap long lines, let me page horizontally to see them
# - if there isn't enough data to page through, just spit it all out and bail
# - don't ring the bell
export LESS="--RAW-CONTROL-CHARS --ignore-case -K -J --LONG-PROMPT --chop-long-lines --QUIET"

# Set up lesspipe, for tricks like looking at compressed files; on
# Debian/Ubuntu, this also calls ~/.lessfilter which can invoke pygmentize etc.
# this exports LESSOPEN and LESSCLOSE.
command -v lesspipe > /dev/null && eval "$(SHELL=/bin/sh lesspipe)"

# if running bash, may need to manually run bashrc
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi
