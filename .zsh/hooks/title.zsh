# set xterm title to path
chpwd() {
    [[ -t 1 ]] || return
    case $TERM in
        *xterm*|rxvt|(dt|k|E)term) print -Pn "\e]2;zsh: %~\a"
            ;;
    esac
}
# set the title at startup
chpwd
