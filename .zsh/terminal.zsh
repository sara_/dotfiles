# if stdin (that's "0") is a terminal...
if [ -t 0 ]; then
    # -ixon: don't freeze terminal when Ctrl-S is entered
    stty -ixon
fi
